﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class BootStrapper 
{
    [RuntimeInitializeOnLoadMethod]
    public void Init() 
    {
        Locator.Init();

        Locator.locatorInstance.RegisterController<IController>(new SoundController());

        SceneManager.LoadSceneAsync(0);
    }
}
